console.log("Mabuhay!");


//JS - is a loosely type Programming Language

// alert("Hello Again");

console. log ( "Hello World!" ) ;

console.
log
(
	"Hello World!"
);

//[SECTION] - Making Comments in JS

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code


/*  
	1. Single-Line comment - Ctrl + /
	2. Multi-Line comment - Ctrl + Shift + /

 */

 // [SECTION] - Variables
 // used to contain data
 // usually stored in a computer's memory.

 // Declaration of variable


// Syntax --> let variableName;
 let myVariable;

 console.log(myVariable);
 
// Syntax --> let variableName variableValue
let mySecondVariable = 2;

 console.log(mySecondVariable);

let productName = "desktop computer";
console.log(productName);

//Reassigning value - let

let friend;

friend = "Kate";
friend = "Jane";

console.log(friend);

//Syntax const variableName variableValue


//Reassigning value - const
//Const holds permanent values and cannot be reassigned.
const pet = "Bruno";
// pet = "Lala"; //error
console.log(pet);

//const hoursPerDay = 24;

// Local and Global Variables

let outerVariable = "hello"; //This is a global variable

{
	let innerVariable = "Hello World!"; //Local variable
	console.log(innerVariable);
}

console.log(outerVariable);

const outerExample = "Global Variable";

{
	const innerExample = "Local variable";
	console.log(innerExample);
}

console.log(outerExample);


//Multiple Declaration

let productCode = "DC017", productBrand = "Dell";

/*let productCode = "DC017";
let productBrand = "Dell";*/

console.log(productCode, productBrand);

// [SECTION] Data Type

//Strings
// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
// Strings in JavaScript can be written using either a single (') or double (") quote

let country = 'Philippines';
let province = "Metro Manila";


//Concatenating Strings
// Multiple string values can be combined to create a single string using the "+" symbol

let fullAdress = province + ", " + country;
console.log(fullAdress);

let greeting = "I live in" + ", " + country;
console.log(greeting);

// The escape character (\) in strings in combination with other characters can produce different effects
// "\n" refers to creating a new line in between text

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

// Using the double quotes along with the single quotes can allow us to easily include single quotes in texts without using the escape character

let message = "John's employees went home early";
message = 'John\'s employees went home early';
message = "John\'s employees went \"home early\"";
console.log(message);

//Numbers
let headcount = 26;
console.log(headcount);

//Decimal Numbers or Fractions
let grade = 98.7;
console.log(grade);

//Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

//Combining strings and integers/numbers
console.log("My grade last sem is " + grade);

//Bollean
// Boolean values are normally used to store values relating to the state of certain things

let isMarried = false;
let inGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

//Arrays
// Arrays are a special kind of data type that's used to store multiple values
// Arrays can store different data types but is normally used to store similar data types
// let/const arrayName = [elementA, elementB, elementC, ...]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

//Array with different Data Types
let details = ["John", "Smith", 32, true];
console.log(details);

//Objects
// Objects are another special kind of data type that's used to mimic real world objects/items
// Syntax
		    // let/const objectName = {
		    //     propertyA: value,
		    //     propertyB: value,
		    // }

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contacts: ["09123456789", "09999999999"],
	address: { //nested object
		houseNumber: "345",
		city: "Manila"
	}
}

console.log(person);

//Re-assigning value to an array
const anime = ["one piece", "one punch man", "Attack on Titan"];
anime[1] = "Kimetsu no Yaiba";

console.log(anime);

